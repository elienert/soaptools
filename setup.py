import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()
    
setup(
    name = "soaptools",
    version = "0.0.1",
    author = "Adam Connor/ASMP-INT",
    author_email = "adam.connor@austin.utexas.edu",
    description = ("Useful soap-related utilities"),
    license = "Copyright 2015 The University of Texas",
    url = "https://code.its.utexas.edu/gitblit/summary/?r=ASMP_INT/soaptools.git",
    packages=find_packages(),
    install_requires = ('lxml>=3.4.4', 'requests>=2.8.1'),
    test_suite = "tests.suite",
    long_description = read("README.md"),
    classifiers=["Development Status :: 2 - Pre-Alpha"],
)