# SoapTools
## Usage
At this point, the only useful piece in SoapTools is SoapRequest. Here's an example use:

```
from soaptools.soap_request import SoapRequest, SoapRequestError

my_xml = """
         <bsvc:Get_Job_Families_Request bsvc:version="v24.1">
         </bsvc:Get_Job_Families_Request>
         """
ns = {'bsvc': 'urn:com.workday/bsvc'}
url = ('https://wd2-impl-services1.workday.com/ccx/service'
       '/utaustin3/Human_Resources/v25.0'
)
username = &lt;some username&gt;
password = &lt;some password&gt;

sr = SoapRequest(url, username, password, nsmap=ns)
sr.append_to_body(my_xml)

response = sr.send_request()

# do your thing
```

### Notes
1. The response returned is a [Python Requests library](http://docs.python-requests.org/en/latest/) response.
2. You don't have to use a template; you can build the XML yourself using lxml if you prefer. See test_soap_request.py.
3. You pass a debugging function into send_request to get more information, e.g., `sr.send_request(debug_func=print)` would have printed details of the request and response.

## Dependencies
This library has the following dependencies:

1. lxml >= 3.4.4
2. requests >= 2.8.1

Other versions of these library may work but were not tested.

## Tests
If you want to run the tests (e.g, via `python setup.py test`), you also need PyYAML >= 2.8.1 installed. It will look for a configuration file
named test_config.yaml. If the environment variable SOAP_TEST_CONFIG
exists, it will look in that directory only. Otherwise, it will look
first in the current directory; if it doesn't find test_config.yaml there,
it will prompt you for input.

Look at test_config_example.yaml for the example structure, and then
fill in the appropriate values.

If you have nose installed, you can also run the tests via
`nosetests -w tests -s`. Nose provides more options for running individual tests. Note that `-w tests` will make the tests directory the working directory, so it will change the default location of test_config.yaml.