"""
This module dynamically builds up the list of tests (assigned to the "suite"
variable) by finding the TestCases and calling their suite() methods.
This allows us to skip tests if pre-requisites (like valid login credentials)
are unmet.
"""

from inspect import isclass
import os
import pkgutil
from unittest import TestCase, TestSuite

test_dir = os.path.dirname(__file__)
suite = TestSuite()
places = [test_dir]

for loader, module_name, is_pkg in pkgutil.walk_packages(places):
    module = __import__('tests.' + module_name)

    # at this point, the submodule is imported into the __init__.py's
    # global namespace, so we need to reference it from there
    # (module references __init__.py)
    nv_dict = globals()[module_name].__dict__
    for name, value in nv_dict.items():
        if isclass(value):
            if issubclass(value, TestCase):
                if hasattr(value, 'suite'):
                    try:
                        s = value.suite()
                        if isinstance(s, TestSuite):
                            suite.addTest(s)
                    except:
                        pass
